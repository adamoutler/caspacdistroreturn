/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package distroreturn;

import com.google.gson.Gson;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author adamoutler
 */
public class ServerSideBuildPropRecognizer {

    static String BUILDPROP;
    static String localRepo;
    static ArrayList<MandatoryThread> searches = new ArrayList<>();
    final static String REPO = "http://builds.casual-dev.com/CASPAC";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new ServerSideBuildPropRecognizer().doWork(args);
        System.exit(0);
    }


    public void doWork(String[] args) {
        if (args.length < 1) {
            System.out.println("args: path/to/CASPACRepo path/to/build.prop");
            return;
        }
        localRepo = args[0];

        if (args.length > 1) {
            setBuildPropFromArg1(args);
        } else {
            setBuildPropFromLocalTestPackage();
        }

        List x = getPackagesList();
        for (Object name : x) {
            System.out.println(name);
        }
        Gson g = new Gson();
        System.out.println();
        System.out.println("----START JSON OUTPUT----");
        System.out.println(g.toJson(x));
        System.out.println("-----END JSON OUTPUT-----");

    }

    private void setBuildPropFromLocalTestPackage() {
        URL resource;
        resource = getClass().getClassLoader().getResource("serversidebuildproprecognizer/build.prop");
        try {
            BUILDPROP = convertStreamToString(resource.openStream());
        } catch (IOException ex) {
            Logger.getLogger(ServerSideBuildPropRecognizer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void setBuildPropFromArg1(String[] args) {
        try {
            BUILDPROP = convertStreamToString(new FileInputStream(args[1]));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ServerSideBuildPropRecognizer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    public String[] recursiveFolderSearch() {

        Set<String> availableURLs = new TreeSet<>();
        //get initial filelist
        searchFolder(localRepo, availableURLs);
        //dispatchThreads( searchFolder("files/").folders);

        int threadsComplete = 0;
        while (threadsComplete < searches.size()) {
            threadsComplete = 0;
            for (MandatoryThread search : getSearches()) {
                if (!search.isComplete()) {

                    System.out.println("Waiting for " + search.getName());
                    search.waitFor();
                    System.out.println("Done Waiting " + search.getName());
                } else {
                    threadsComplete++;
                }
            }

        }
        System.out.println(availableURLs);
        availableURLs.remove("http://builds.casual-dev.com/");
        availableURLs.remove("http://builds.casual-dev.com/null");
        return availableURLs.toArray(new String[availableURLs.size()]);
    }

    /**
     * adds a thread to the searches list
     *
     * @param t thread to be added
     */
    synchronized static void addSearch(MandatoryThread t) {
        searches.add(t);
    }

    /**
     * gets an array of MandatoryThreads
     *
     * @return array of MandatoryThreads
     */
    MandatoryThread[] getSearches() {
        return searches.toArray(new MandatoryThread[searches.size()]);
    }

    public List getPackagesList() {
        ArrayList<String> cp = new ArrayList<>();
        String[] list = recursiveFolderSearch();
        for (String item:list){
            cp.add(item);
        }
        System.out.println("Found " + cp.size() + " valid CASPACs");
        return cp;

    }

    private void dispatchThreads(final String[] availableFolders, final Set<String> availableURLs) {
        for (final String folder : availableFolders) {
            if (folder == null || folder.equals("null") || folder.isEmpty()) {
                return;
            }
            MandatoryThread t = new MandatoryThread(new Runnable() {
                @Override
                public void run() {
                    searchFolder(folder, availableURLs);
                }
            });
            addSearch(t);
            t.start();
            t.setName(folder);
        }
    }

    /**
     * Searches a folder if the folder is not in the blacklist
     *
     * @param folder folder to perform work on
     * @param availableURLs reference to URL list
     */
    private void searchFolder(String folder, Set<String> availableURLs) {
        String[] worklist = new String[]{};
        System.out.println("Checking blacklist status on "+folder);
        if (!isBlacklisted(folder)) {
            try {
                worklist = folderList(folder, availableURLs);
                for (String name : worklist) {
                    System.out.println("Folder: " + name);
                }
            } catch (IOException ex) {
                Logger.getLogger(ServerSideBuildPropRecognizer.class.getName()).log(Level.SEVERE, null, ex);
            }
            dispatchThreads(worklist, availableURLs);
        }
    }

    /**
     * downloads the folder b.prop if available and checks it for whitelist then
     * checks for blacklist.
     *
     * @param folder online folder to check
     * @return true if folder is blacklisted
     */
    private static boolean isBlacklisted(String folder) {
        boolean blacklisted;
        Properties buildprop = new Properties();
        try {
            File f = new File(folder + "/b.prop");
            buildprop.load(new FileInputStream(f));
        } catch (IOException ex) {
            //no b.prop to read so we won't blacklist
            return false;
        }
        //set the blacklist if there is a whitelist
        blacklisted = !buildprop.getProperty("w[0]", "").isEmpty();

        //parse whitelist in properties file
        String checkValue;
        int i = -1;
        while (!(checkValue = buildprop.getProperty("w[" + ++i + "]", "")).isEmpty()) {
            if (checkValue.toLowerCase().equals("all")){
                return false;
            }
            //unset blacklist if value is detected
            if (BUILDPROP.contains(checkValue)) {
                blacklisted = false;
                break;
            }

        }
        //parse blacklist in properties file
        i = -1;
        while (!(checkValue = buildprop.getProperty("b[" + ++i + "]", "")).isEmpty()) {
            if (BUILDPROP.contains(checkValue)) {
                //if its in there break, no need to parse other values
                blacklisted = true;
                break;
            }
        }
        System.out.println(folder + (blacklisted ? " does not apply to this device." : " is Whitelisted."));
        return blacklisted;
    }

    /**
     * Performs the listing on the folder. This is performed after blacklist
     * Gets folders and files. Adds files to the availableURLs list
     *
     * @param checkFolder folder to do work on
     * @param availableFolders reference to the master URL list
     * @return new work items to be addressed.
     * @throws MalformedURLException {@inheritDoc}
     * @throws IOException {@inheritDoc}
     */
    private String[] folderList(String checkFolder, Set<String> availableFolders) throws MalformedURLException, IOException {

        //Open the URL with folder search query.
        File check = new File(checkFolder);

        ArrayList<String> folders = new ArrayList<>();
        ArrayList<String> files = new ArrayList<>();
        for (File folder : check.listFiles()) {
            if (folder.isDirectory()) {
                folders.add(folder.getCanonicalPath());
            } else if (folder.isFile()) {

                String file = folder.toString();
                if (!file.endsWith("b.prop") && (!file.endsWith(".properties"))) {
                    file = file.replace("\\/", "/");
                    availableFolders.add(file);
                    System.out.println("Available File: " + file);
                }
            }

        }
        return folders.toArray(new String[folders.size()]);
    }
}